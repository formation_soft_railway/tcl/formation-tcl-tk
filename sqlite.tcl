# BDD SQLite
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/Sqlite3Cmd/sqlite3.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/SqliteCmd/sqlite3.htm
# 	doc sqlite3 : https://www.sqlite.org/tclsqlite.html

package require sqlite3

puts "-----------------------------------"

if {[catch {
		cd data
		
		# (Creation et) Ouverture d'une BDD zoo
		sqlite3 db zoo.sqlite
		
		# Creation d'une table animaux
		db eval {CREATE TABLE IF NOT EXISTS animaux(id INTEGER PRIMARY KEY,
					nom TEXT, espece TEXT)}
		# Ajout d'un element (Bob,Tigre) a la table
		db eval {INSERT INTO animaux(nom,espece)VALUES('Bob','Tigre')}
		
		set animaux [list \
			[list Franklin Tortue] \
			[list Ann Girafe] \
			[list Helen Crocodile] \
		]
		puts "Insertions dans la table Animaux de la BDD zoo.sqlite :"
		foreach animal $animaux {
			set nom [lindex $animal 0]
			set espece [lindex $animal 1]
			puts "  - $nom "
			# /!\ Ne pas mettre $ ici, c'est l'interpreteur sqlite qui 
			# 	  s'occupera du remplacement
			db eval {INSERT INTO animaux(nom,espece)VALUES($nom,$espece)}
		}
		
		db eval {SELECT COUNT (*) AS c FROM animaux} {
			puts "Il y a $c animaux."
		}
		
		db eval {SELECT * FROM animaux} {
			#foreach animal $res
			puts "| $id : $nom ($espece)"
		}
		
		# Fermeture de la BDD zoo
		db close
	} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}

puts "-----------------------------------"