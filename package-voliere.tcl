# declaration de package
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/package.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/package.htm

package require Tcl 8.5
# a : -2.  -	b = -1.
# package provide Voliere 1.2a1 => 1.2-2.1
# package provide Voliere 1.2a2 => 1.2-2.2
# package provide Voliere 1.2a3
# package provide Voliere 1.2b1 => 1.2-1.1
# package provide Voliere 1.2
package provide Voliere 1.2.1

namespace eval Voliere {
	variable especes [list Aigles Vautours Milans Peroquets]
	
	namespace export afficher ajouter
}

proc Voliere::afficher {} {
	variable especes
	puts "Notre voliere accueille : "
	foreach espece $especes {
		puts "  - des [string tolower $espece]"
	}
}

proc Voliere::ajouter {oiseau} {
	variable especes
	lappend especes $oiseau
}

# Voliere::afficher


# ------------------------------------------------------------------------------
# Dans une console 
# ------------------------------------------------------------------------------
# > tclsh
# % pkg_mkIndex . package-voliere.tcl
# 		=> Fabrique un fichier pkgIndex.tcl dans le repertoire courant
# 		=> Pour partager le package, distribuer un zip contenant le fichier ou
# 		   est declare le package (ici package-voliere.tcl) + pkgIndex.tcl
# ------------------------------------------------------------------------------