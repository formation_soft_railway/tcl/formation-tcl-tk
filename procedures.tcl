# procedures du zoo
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/proc.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/proc.htm

puts "-----------------------------------"

proc sayHello {} {puts "Hello Zoo !"}
sayHello

# excl est un parametre optionnel
#proc sayHelloAnimal {name {excl !}} { 
proc sayHelloAnimal {{name "mon Petit"} {excl !}} { 
	puts "Hello $name $excl" 
}
sayHelloAnimal
sayHelloAnimal "Ann la girafe"
sayHelloAnimal "Tom le tigre" !!!

proc sayHelloMutliple args {
	puts "-- Hello everyone ! --"
	foreach animal $args {
		sayHelloAnimal $animal
	}
}
sayHelloMutliple Ann Tom Carol

proc getCri {animal} {
	proc getCriFauve {} {return rugit}
	switch $animal {
		tigre { return [getCriFauve] }
		girafe { return meugle }
		faucon { return huit }
		default {return "... euh.. fait du bruit ?"}
	}
}
puts "La giraffe [getCri girafe]"
puts "La tortue [getCri tortue]"
puts "Le tigre [getCri tigre]"

proc tarifPour {visiteurs} {
	# Sans le mot cle "global", la procedure ne va pas chercher "tarif" dans le 
	# namespace global. On obtient une erreur a l'appel :
	# can't read "tarif": no such variable
	#global tarif
	#return [expr $tarif*$visiteurs]
	
	# tarifLoc est un alias (cf. references en C++)
	# le lien est fait avec une variable a un niveau juste au-dessus (dans 
	# l'appelant), avec global, la valeur recuperee est dans le niveau 0
	upvar tarif tarifLoc
	return [expr $tarifLoc*$visiteurs]
}
# La ligne suivante est inutile. Elle sert uniquement a but informatif
global tarif
set tarif 8.4
puts "Tarif pour 2 : [tarifPour 2] €"

proc decr {nomVariable {decrement 1}} {
	upvar $nomVariable x
	set x [expr $x - $decrement]
}

proc afficheInfos {} {
	set tarif 10.3
	decr tarif
	puts "W.E. special pour 4 : [tarifPour 4]"
}
afficheInfos

	

puts "-----------------------------------"