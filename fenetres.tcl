# Fenetres TK
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TkCmd/contents.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TkCmd/contents.htm

# * Exemple :
#	Fenetre				.
#	\- Panneau1			.panneau1
#		\- Label1		.panneau1.label1
#	\- Panneau2			.panneau2
#		\- Label2		.panneau1.label2

package require Tk

puts "-----------------------------------"
puts "Creation de la fenetre"

# Lorsqu'on utilise TK, une fenetre est automatiquement creee

proc test_fenetres {} {
	# Ici, deux autres fenetres sont creees. La fermeture de la fenetre principale 
	# entraine la fermeture des deux fenetres suivantes
	tk::toplevel .fenetre2
	wm title .fenetre2 "Fenetre numero 2"
	wm geometry .fenetre2 400x100

	tk::toplevel .fenetre3
	# largeur : 800px, hauteur : 500 px, a 100px de la gauche, a 0 de haut
	wm geometry .fenetre3 800x500+100+0
	# -alpha 0.5 rend la fenetre semi-transparente
	wm attributes .fenetre3 -alpha 0.5 -topmost 1
}

# Modification de la geometrie de la fenetre principale
wm title . "Alerte du zoo"
wm geometry . 600x600

# ------------------------------------------------------------------------------
# 		Panneau1
# ------------------------------------------------------------------------------

# Cree un panneau dans la fenetre principale avec la hauteur, la largeur 
# indiques
# tk::frame .panneau1 -width 580 -height 280 -background red
# 	Option :
#		-relief : raised (sureleve), sunken (enfonce)
#		-bd : largeur de la bordure
# tk::frame .panneau1 -width 580 -height 280 -relief raised -bd 2
tk::frame .panneau1 -height 280 -relief raised -bd 2
# On reprecise la largeur et la hauteur pour que le panneau garde une taille 
# fixe et ne s'agrandisse pas par rapport a son contenu
place .panneau1 -x 10 -y 10 -width 580 -height 280
# pack .panneau1 -side top -fill x

# tk::label .panneau1.labelNom -text {Nom :}
# Placement absolu (dans le panneau1)
# place .panneau1.labelNom -x 10 -y 10

# Autres composants : button, entry, text(bloc), checkbutton, radiobutton, 
# spinbox, listbox

# Dans le panneau1, de haut en bas :
# Titre
# [ ]
# Lieu
# [ ]
# Informations [ ]
#
#
# [] Urgent
# [ Envoyer ]

# --- Titre ---
tk::label .panneau1.titrelabel -text "Titre"
# set y 10
# place .panneau1.titrelabel -x 10 -y $y
# Placement dans une grille
grid .panneau1.titrelabel -column 0 -row 0

tk::entry .panneau1.titre -textvariable titre
# set y [expr $y+30]
# place .panneau1.titre -x 10 -y $y
grid .panneau1.titre -column 1 -row 0

# --- Lieu ---
tk::label .panneau1.lieulabel -text "Lieu"
# set y [expr $y+30]
# place .panneau1.lieulabel -x 10 -y $y
grid .panneau1.lieulabel -column 0 -row 1

tk::entry .panneau1.lieu -textvariable lieu
# set y [expr $y+30]
# place .panneau1.lieu -x 10 -y $y
grid .panneau1.lieu -column 1 -row 1

# --- Informations ---
tk::label .panneau1.infoslabel -text "Informations"
# set y [expr $y+30]
# place .panneau1.infoslabel -x 10 -y $y
grid .panneau1.infoslabel -column 0 -row 2

tk::text .panneau1.infos
# set y [expr $y+30]
# place .panneau1.infos -x 10 -y $y  -height 40 -width 500
# -columnspan 2 : le composant s'etale sur 2 colomnes 
grid .panneau1.infos -column 0 -row 3 -columnspan 2
# S'il reste de la place dans la fenetre, le composant prend toute la place restante
grid columnconfigure .panneau1 .panneau1.infos -weight 1
grid rowconfigure .panneau1 .panneau1.infos -weight 1

# --- Urgent ---
tk::checkbutton .panneau1.urgent -text "Urgent" -variable urgent
# set y [expr $y+45]
# place .panneau1.urgent -x 10 -y $y
grid .panneau1.urgent -column 0 -row 4

# --- Envoyer ---
# Lorsque le bouton est clique, on execute on_btnEnvoyer
tk::button .panneau1.envoyer -text "Envoyer" -command on_btnEnvoyer
# set y [expr $y+30]
# place .panneau1.envoyer -x 10 -y $y
grid .panneau1.envoyer -column 0 -row 5


# ------------------------------------------------------------------------------
# 		Panneau2
# ------------------------------------------------------------------------------

tk::labelframe .panneau2 -width 580 -height 280 -text Informations
place .panneau2 -x 10 -y 310
# pack .panneau2 -fill x

# textvariable : permettra de changer ce qui est afficher dans le resultat
tk::label .panneau2.message -text {Resultat} -textvariable message
place .panneau2.message -x 10 -y 10

# ------------------------------------------------------------------------------
# 		Procedures
# ------------------------------------------------------------------------------
proc on_btnEnvoyer {} {
	global message titre lieu urgent
	
	# Recuperation du contenu de infos
	global .panneau1.infos
	set informations [.panneau1.infos get 1.0 end]
	
	# set message "Ok"
	set message "$message\n----\nEnvoi de \"$titre\" a $lieu"
	set message "$message\n$informations"
	if {$urgent} {
	set message "$message\nC'est urgent !"
	}
}

# Quitter avec Ctrl+q
bind . <Control-KeyPress-q> "exit"
bind . <Key-Return> on_btnEnvoyer

puts "-----------------------------------"