# utilisation d'un package
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/package.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/package.htm

puts "-----------------------------------"

# auto_path Contient les repertoires dans lesquels on ira chercher les packages
#puts $auto_path

# Ajout d'un chemin absolu
#lappend auto_path {C:\Users\vtamimo\Documents\Formation Software Engineering for railway\formation-tcl-tk}
# ou d'un chemin relatif
lappend auto_path "."

# On recupere le package Voliere
package require Voliere

Voliere::afficher
Voliere::ajouter Buses
Voliere::afficher

#puts $auto_path

puts "-----------------------------------"