# Classes et objets
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/class.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/class.htm

puts "-----------------------------------"

oo::class create Animal {
	# Les variables sont privees !
	variable nom
	variable age
	variable espece
	
	# Le 1er constructeur est ecrase par la 2eme declaration
	# constructor {} {puts "par defaut"}
	
	# Constructeur
	constructor {{n ""} {a 0} {e ""}} {
		set nom $n
		set age $a
		set espece $e
	}
	
	destructor {puts "$nom est parti"}
	
	method setNom {n} { set nom $n }
	method setAge {a} { set age $a }
	method setAgeEnMois {m} {my setAge [expr $m/12]}
	method setEspece {e} { set espece $e }
	method afficher {} { puts "$nom ($espece) a $age ans"}
}

# Appel du constructeur avec parametres par defaut
set ann [Animal new]
$ann setNom "Ann"
$ann setAge 23
$ann setEspece Girafe
$ann afficher
# Un objet d'une classe n'est pas une chaine de caractere (contrairement a la 
# majorite des objets en TCL). $ann renvoie ::oo::Obj12
#puts $ann

set tom [Animal new "Tom" 8 Tigre]
$tom afficher
# Appel du destructeur
$tom destroy

puts "-----------------------------------"

# Mixin : ce n'est pas une "vraie" classe. Encadreur ne contient pas afficher.
# Dans un mixin, on met en commun plusieurs methodes. Lorsqu'on appelera 
# encadrer, la fonction ira chercher afficher dans sa classe
oo::class create Encadreur {
	method encadrer {} {
		puts "~~~~~~~~~~~~~~~~~~~~~~~~~~"
		my afficher
		puts "~~~~~~~~~~~~~~~~~~~~~~~~~~"
	}
}

oo::class create Reptile {
	# Indique qu'on herite de la classe Animal
	superclass Animal
	mixin Encadreur
	variable amphibien
	
	# Constructeur
	constructor {{n ""} {a 0} {e ""} {am ""}} {
		# Ne fonctionne pas ! On cree de nomvelles variables au lieu 
		# d'initialiser les parametres declares dans la classe mere
		# set nom $n
		# set age $a
		# set espece $e
		
		# Appel au constructeur de la classe mere grace a next
		next $n $a $e
		set amphibien $am
	}
	
	method setAmphibien {am} { set amphibien $am }
	
	method afficher {} {
		# next
		# permet d'indiquer qu'on va chercher afficher dans Animal (et non dans 
		# Encadreur si une methode du meme nom y avait ete defini)
		nextto Animal
		if {$amphibien} {
		#if {$amphibien eq 1} {}
			puts "  - C'est un amphibien"
		} else {
			puts "  - Ce n'est pas un amphibien"
		}
	}
}

set franck [Reptile new "Franck" 3 Caiman 1]
# $franck afficher
$franck encadrer

puts "-----------------------------------"