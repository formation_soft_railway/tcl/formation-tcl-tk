# -- Listes (exemples) --
# set bob [list lion 12 "enclos 4"]
# lappend notes fa sol
# linsert $notes 0 si
# lset animaux2 2 Aigle
# lreplace $animaux2 0 1 Chat Mouton Lion

# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/contents.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/contents.htm

# llength $animaux2
# lindex $animaux2 1
# foreach a $animaux2 { puts $a }

# --------------------------
# -- Operations sur liste --
# --------------------------

# Preparer une liste de 4 lieux
set lieux [list voliere Mare vivarium terrarium {enclos des singes}]
# Afficher tous les trajets visitant 3 lieux differents

# Pb : n'affiche pas toutes les combinaisons
#for {set i 0} {$i<[llength $lieux]} {incr i} {
#	set i2 [expr ($i+1)%[llength $lieux]]
#	set i3 [expr ($i+2)%[llength $lieux]]
#	puts "[lindex $lieux $i] - [lindex $lieux $i2] - [lindex $lieux $i3]"
#}
puts "-- Trajets possibles --"
for {set i 0} {$i<[llength $lieux]} {incr i} {
	for {set j 0} {$j<[llength $lieux]} {incr j} {
		for {set k 0} {$k<[llength $lieux]} {incr k} {
			if {$i != $j && $i != $k && $j != $k} {
				puts "  - [lindex $lieux $i] - [lindex $lieux $j] - [lindex $lieux $k]"
			}
		}
	}
}
puts "-----------------------------------"

puts "Le vivarium est le lieu num. [lsearch $lieux vivarium]"
# Ne prends pas en compte majuscules et minuscules
puts "Le vivarium est le lieu num. [lsearch -nocase $lieux viVariUm]"
puts "Le vivarium est le lieu num. [lsearch -nocase -ascii $lieux vivarium]"
# Autre options :
# 		-ascii -integer ou -real (les 2 derniers incompatibles avec -nocase)
# 		-sorted : si la liste est deja ordonnee
# 		-all : renvoie la liste de tous les elements trouves (sans, uniquement le 1er)
# 		- start x : commence a l'element x
# 		-exact -glob -decreasing

set lieux_ordonnes [lsort $lieux]
# Options possibles :
# 		-increasing ou -decreasing
# 		-ascii -integer ou -real
# 		-nocase
#set lieux_ordonnes [lsort -ascii -nocase -increasing $lieux]
set lieux_ordonnes [lsort -ascii -increasing -nocase $lieux]
puts $lieux_ordonnes

# chaine = liste de mots
# Les declarations suivantes sont équivalentes a la declaration de lieux
set lieux2 {voliere Mare vivarium terrarium {enclos des singes}}
set lieux3 "voliere Mare vivarium terrarium {enclos des singes}"
foreach lieu $lieux3 {
	puts "- $lieu"
}

# enclos des singes
set premier_lieu [lindex $lieux3 4]
# enclos
set premier_mot [lindex $premier_lieu 0]
puts $premier_mot
puts "Dans cette chaine il y a [llength $premier_mot] mot"
puts "Dans Lions,Tigres,Jaguars : [llength {Lions,Tigres,Jaguars}] mot"

puts "-----------------------------------"

# ---------------------------
# -- Operations sur chaine --
# ---------------------------

# chaines (commandes string)
puts "Dans le 1er lieu, il y a [string length $premier_lieu] caracteres"
puts "Le 1er est : [string index $premier_lieu 0]"
puts "Nettoyé : [string toupper [string trim $premier_lieu 0]]"
puts "Emplacement du dernier d : [string last d [string totitle $premier_lieu]]"

# Tous les lieux qui contiennent un u par ordre alphabetique
puts "Lieux contenant un 'u' : "
foreach lieu $lieux_ordonnes {
#foreach lieu $lieux 
	if {[string first u $lieu] != -1} {
		puts $lieu
	}
}

puts "-----------------------------------"

# ---------------------------
# -------- Tableaux ---------
# ---------------------------

set bob(espece) Lion
set bob(age) 12
set bob(lieu) {Enclos des fauves}

puts "Bob est un $bob(espece)"
# Ne fonctionne pas !
# puts "Bob : $bob"
puts "Bob a [array size bob] proprietes"
#if {[lsearch [array names bob] age]!=-1} {
if {"age" in [array names bob]!=-1} {
	puts "Age de Bob : $bob(age)"
}
# liste les cles du tableau
puts [array names bob]

puts "-----------------------------------"

# ---------------------------
# ------ Dictionnaires ------
# ---------------------------
set ann [dict create espece Giraffe age 22 lieu {Enfuie}]
dict append ann provenance Kenya
dict set ann provenance Tanzanie

puts "Ann : $ann"
puts "Ann a [dict size $ann] proprietes : [dict keys $ann]"

if {[dict exists $ann lieu]} {
	puts "Lieu de Ann : [dict get $ann lieu]"
}

# A partir des lieux, afficher une correspondance :
# lettre initiale => nombre de lieux (>0)
set lieux2 [list Menagerie Voliere Aquarium {Enclos des fauves} {Enclos des singes}]
# exemple : A:1, E:2, M:1, V:1
set correspondance [dict create]
foreach lieu [lsort $lieux2] {
	set lettre [string index $lieu 0]
	#puts "la lettre : $lettre"
	if {[dict exists $correspondance $lettre]} {
		dict set correspondance $lettre [expr 1+[dict get $correspondance $lettre]]
		#puts "Maj lettre"
	} else {
		dict append correspondance $lettre 1
		#puts "ajout nouvelle lettre"
	}
}
puts "Correspondance : $correspondance"

# -- Liste a deux dimensions --
#visiteurs par mois et jours
set nbvisiteurs [list \
	[list 213 119 110 321 198] \
	[list 110 232 321 455 198] \
	[list 111 146 191 235 319]
]
# ou set nbvisiteurs { {213 119 ...} {...} {...} }
puts "visiteurs le 1er janvier : [lindex $nbvisiteurs 0 0]"
puts "visiteurs le 3 fevrier : [lindex $nbvisiteurs 1 2]"

set enclos [list \
	[dict create Nom Fauves Taille 23400] \
	[dict create Nom Herbivores Taille 9100] \
	[dict create Nom Singes Taille 6700]
]
if {[llength $enclos]>2 && [dict exists [lindex $enclos 2] Taille]} {
	puts "Superficie de l'enclos des singes : [dict get [lindex $enclos 2] Taille]"
}

puts "-----------------------------------"

# ---------------------------
# --------- Horloge ---------
# ---------------------------

set start [clock microseconds]
# Heure systeme : Temps ecoule depuis le 1er janvier 1970
puts "Il est : [clock seconds]"
puts "Il est : [clock microseconds]"

set demain [clock add [clock seconds] 1 days]
puts "Demain : $demain"
puts "Demain : [clock format $demain -format {%Y-%m-%d %H:%M:%S}]"

set end [clock microseconds]
puts "Temps ecoule : [expr $end-$start] us"

#time {puts [expr sin(3)]}
puts "Temps pour afficher sin(3): [time {puts [expr sin(3)]} 100]"
# 377.155 microseconds per iteration
puts "Temps (complexe) pour afficher sin(3): [timerate {puts [expr sin(3)]} 100]"
# Tps/execution - nb d'execution dans 100 ms - nb d'exec/sec - tps d'exec total (hors mesure)
# 358.172 µs/# 285 # 2792.0 #/sec 102.079 net-ms

puts "-----------------------------------"