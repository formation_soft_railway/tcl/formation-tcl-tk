#threads
# Documentation : file:///[Chemin_vers_tcl]/tcl_tk/share/doc/tcl/ThreadCmd/contents.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/ThreadCmd/contents.htm

package require Thread

# puts "-----------------------------------"

# Pour de grands nombres, l'execution de la fonction peut etre potentiellement 
# très long. La complexite est en O(exp(n))
# proc nblapins {mois} {
	# le 1er mois : 1 lapin - le 2eme : 2 lapins
	# if {$mois<3} {
		# return $mois
	# } else {
	# 3 mois et + : appel recursif a la fonction - 
	# nblapins = 3*(nblapins(mois-1)+nblapins(mois-2))/2
		# return [expr 3*([nblapins [expr $mois-1]]+[nblapins [expr $mois-2]])/2]
	# }
# }

# puts "Au bout de 6 mois : [nblapins 6]"

# set threadcalcul [thread::create -joinable {

	# Les procedures declarees en dehors du thread lui sont inconnues
	# proc nblapins {mois} {
		# if {$mois<3} {
			# return $mois
		# } else {
			# return [expr 3*([nblapins [expr $mois-1]]+[nblapins [expr $mois-2]])/2]
		# }
	# }

	# puts "Au bout de 2 ans et 8 mois : "
	# puts "[nblapins 32]"
# }]
# puts "Calcul"
# Lie l'affichage des "." a l'execution de la fonction. Le for s'arrete lorsque 
# le thread est fini
# for {set i 0} {[thread::exists $threadcalcul]} {incr i} {
# for {set i 0} {$i<10} {incr i} {}
	# nonewline => pas de retour a la ligne apres l'affichage
	# puts -nonewline "."
	# N'attend pas que la ligne soit complete pour l'afficher
	# flush stdout
	# attente pendant 300 ms
	# after 300
# }
# puts ""
# Le programme attendra la fin de l'execution du thread pour s'arreter
# thread::join $threadcalcul

puts "-----------------------------------"

# tsv : thread shared variable - rend la variable accessible dans le thread
tsv::set lapins resultat 0
tsv::set lapins2 tropLong 0
set threadcalcul2 [thread::create -joinable {

	proc nblapins {mois} {
		if {$mois<3} {
			return $mois
		# Interrompt la fonction si on a passe plus de 10s dans la fonction
		} elseif {[tsv::get lapins2 tropLong]} {
			return Inf
		} else {
			return [expr 3*([nblapins [expr $mois-1]]+[nblapins [expr $mois-2]])/2]
		}
	}

	# Verrouille l'acces a la variable. Empeche un autre thread d'acceder a la 
	# variable pendant qu'elle est utilisee ici
	tsv::lock lapins {
		tsv::set lapins resultat [nblapins 36]
	}
}]
puts "Calcul"
puts "Au bout de 2 ans et 8 mois : "

for {set i 0} {[thread::exists $threadcalcul2]} {incr i} {
	puts -nonewline "."
	flush stdout
	after 500
	if {$i==20} {
		tsv::set lapins2 tropLong 1
		puts "\nTimeout error !"
	}
}

tsv::lock lapins {
	puts "\n[tsv::get lapins resultat]"
}

puts "-----------------------------------"