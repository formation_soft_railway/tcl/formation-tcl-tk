# fichiers
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/contents.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/contents.htm

puts "-----------------------------------"

# erreurs
# Bloc de recuperation d'erreur
if {[catch { [expr 3/0] } err]} {
	puts "Erreur : $err"
}

# Declenchement d'exceptions
if {[catch { error "Plus de cacahuetes"} err]} {
	puts "Erreur : $err !"
}

# Declenchement avec d'autres parametres (optionnels)
if {[catch { 
		error "Plus de cacahuetes" "Enclos des singes" 10
		} err]} {
	puts "Erreur : $err ! - $errorInfo ($errorCode)"
}

puts "-----------------------------------"
if {[catch {

		# ----------------------------------------------------------------------
		# 		Ecrire dans un fichier
		# ----------------------------------------------------------------------
		# Creation (et) Deplacement vers le dossier data
		if {![file exists data]} {
			file mkdir data
		}
		cd data
		puts "Repertoire en cours : [pwd]"
		
		# Option -nocomplain : pas d'erreur si la liste est vide
		puts "Le repertoire contient les fichiers :"
		foreach fichier [glob -nocomplain *.*] {
			puts "- $fichier"
		}
		
		# Creation/Ouverture d'un fichier au nom de log210408.txt en ecriture 
		# (option a)
		set nomFichier "log[clock format [clock seconds] -format %y%m%d].txt"
		puts "Creation de $nomFichier"
		set f1 [open $nomFichier a]
		
		# Ecriture de 2021-04-08 15:47:56 ok dans le fichier
		puts $f1 "[clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}] ok"
		
		# Fermeture du fichier
		close $f1
		
		# ----------------------------------------------------------------------
		# 		Lire un fichier
		# ----------------------------------------------------------------------
		
		# Ouverture du fichier en lecture (option r)
		set fichier [open $nomFichier r]
		set nblignes 0
		while {[gets $fichier ligne] >=0} {
			incr nblignes
		}
		puts "$nblignes lignes de log dans le fichier d'aujourd'hui"
		
	} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}


puts "-----------------------------------"