Formation TCL TK

Date : 06/04/2021
TCL version : 8.6.11
Git prof : https://gitlab.com/mbalib/formation-tcl-210406
Documentation : %USER%/tcl_tk/share/doc/tcl//contents.htm
	 en ligne : https://www.tcl.tk/man/tcl/contents.htm

-- Commandes TCL --
> wish
> tclsh monFichier.tcl 

> tclsh
% source monFichier.tcl 
% exit

% source -encoding utf-8 monFichier.tcl 
> tclsh -encoding utf-8 monFichier.tcl 

-- Run (Notepad ++) --
C:\Users\vtamimo\tcl_tk\bin\tclsh -encoding utf-8 "$(FULL_CURRENT_PATH)"

-- Commandes Git --
git clone https://gitlab.com/formation_soft_railway/formation-tcl-tk.git
cd formation-tcl-tk

git config --global user.name "user name"
git config --global user.email "example@email.com"

git add myFile.txt
git commit -am "New file"
git push