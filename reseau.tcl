# Reseau - spckets
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/ThreadCmd/thread.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/ThreadCmd/contents.htm

package require Thread

# Protocole type TCP IP

puts "-----------------------------------"

# ------------------------------------------------------------------------------
# 		Thread serveur
# ------------------------------------------------------------------------------
set threadserver [thread::create -joinable {
	set stop 0
	proc on_message {connexion} {
		gets $connexion nom
		# Si le message recu est "STOP", on met stop a 1, ce qui va permettre de
		# stopper le serveur (voir ligne 52)
		if {$nom eq "STOP"} {
			# Rend stop accessible en dehors de la procedure actuelle
			global stop
			set stop 1
		} else {
			switch $nom {
				Ann {set age "Ann a 22 ans"}
				Bob {set age "Bob a 8 ans"}
				Cece {set age "Cece a 14 ans"}
				Dan {set age "Dan a 38 ans"}
				default {set age "Je ne connais pas $nom ?"}
			}
			puts $connexion " * $age"
			flush $connexion
		}
	}

	proc on_connect {connexion adresse port} {
			puts "Client connecte : $adresse ($port)"
			puts " * Bonjour mon petit"
			puts $connexion "Il est [clock format [clock seconds] -format \
							 {%Y-%m-%d %H:%M:%S}] au Zoo"
			# Permet l'envoi de la donnee une fois qu'elle a ete preparee
			flush $connexion
			# Boucle d'evenement (similaire au systeme d'interruption)
			fileevent $connexion readable "on_message $connexion"
	}
	
	# Ouverture d'un socket. On ecoute sur le port 11000. Lorsqu'un client se
	# connecte, on declenche la fonction on_connect
	socket -server on_connect 11000
	puts "Le serveur a demarre"	
	
	# Ne s'arrete pas (jusqu'a un Ctrl+C dans la console)
	# Pour arreter le serveur
	# > Ctrl+C 
	# vwait forever
	# Attent que stop passe a 1
	vwait stop

	puts "serveur arrete"
}]

# ------------------------------------------------------------------------------
# 		Thread principal (ici client)
# ------------------------------------------------------------------------------

after 1000
puts "..."
# Connexion d'un client au serveur (sur le port serveur 11000)
set client [socket localhost 11000]
# Reception de la reponse du serveur
gets $client reponse
puts " * Reponse du serveur : $reponse"

puts "  - Bonjour, grand maitre"

puts "..."
after 500
puts "    ~ Quel age a Bob ?"
puts $client "Bob"
flush $client
gets $client reponse
puts $reponse

puts "..."
after 500
puts "    ~ Quel age a Lolo ?"
puts $client "Lolo"
flush $client
gets $client reponse
puts $reponse

puts "..."

after 1000

#close $client
# Envoi de la demande d'arret au serveur
puts $client STOP
flush $client

thread::join $threadserver

# Ouvrir une nouvelle console cmd
# > netstat -an
# 		=> Dans les resultats, on devrait avoir les lignes :
# TCP    0.0.0.0:11000          0.0.0.0:0              LISTENING
# ...
# TCP    [::]:11000             [::]:0                 LISTENING

puts "-----------------------------------"