# namespaces
# ---
# abc::x => x dans le namespace abc
# ::x => x dans le namespace racine
# x => x dans le namespace en cours
# ---
# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/namespace.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/namespace.htm

puts "-----------------------------------"

namespace eval Aquarium {
	# Les elements ci-dessous appartiennent au namespace Aquarium
	
	# On ne peut pas utiliser de set ici
	::variable longueur 27
	variable largeur 18
	variable profondeur 6
	
	# Indique ce qui sera exporte
	namespace export affiche
}

# Avec Aquarium:: on indique que la procedure getVolume fait partie de ce namespace
proc Aquarium::getVolume {} {
#proc getVolume {} {}
	#::variable Aquarium::longueur 
	# variable Aquarium::largeur 
	# variable Aquarium::profondeur
	
	# Inutile de prefixer avec Aquarium:: car on est deja dans le namespace
	::variable longueur 
	variable largeur 
	variable profondeur
	::return [::expr $longueur*$largeur*$profondeur]
}
#proc affiche {} {}
proc Aquarium::affiche {} {
	::puts "Notre aquarium : [getVolume] m3 !"
}

::puts "Informations :"

# :: indique qu'on va chercher la procedure a la racine des declaration
# Equivalent a quand on ne met rien (i.e. pas de ::)
Aquarium::affiche

# On importe tout ce qui est exportable du namespace Aquarium
namespace import Aquarium::*
affiche

puts "-----------------------------------"