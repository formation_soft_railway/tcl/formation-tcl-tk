# Documentation : file:///[chemin_vers_tcl]/tcl_tk/share/doc/tcl/TclCmd/contents.htm
# 	   en ligne : https://www.tcl.tk/man/tcl/TclCmd/contents.htm

puts "Bienvenue à la 1ère appli Zoo"

#puts "hello
#wordl"
#puts "hello"; puts "wordl 2"

# la place est a 8.50€, mais a partir de 5 personnes,
# c'est a 6.3
# afficher le prix pour 1 et pour 6

set place14 8.5
set place5plus 6.3

set prix_un $place14
set prix_6 [expr $place14*4+$place5plus*2]
puts "Une personne : $prix_un €, 6 personnes : $prix_6 €"

puts "Nombre de visiteurs : "
#lecture entree console
gets stdin visiteurs

# Afficher le tarif en fonction du nombre de visiteurs,
# en offrant du popcorn (1 paquet pour 3 visiteurs, si au moins 4 visiteurs)
if {$visiteurs < 5} {
	set prix [expr $visiteurs*$place14]
} else {
	set prix [expr 4*$place14+($visiteurs-4)*$place5plus]
}
if {$visiteurs > 3} {
	set popcorns [expr $visiteurs / 3]
} else {
	set popcorns 0
}

# -- Autre solution --
#if  {$visiteurs<4} {
#	set prix [expr $visiteurs*$place14]
#	set popcorns 0
#} else {
#	if {$visiteurs<5} {
#		set prix [expr $visiteurs*$place14]
#	} else {
#		set prix [expr 4*$place14+($visiteurs-4)*$place5plus]
#	}
#	set popcorns [expr $visiteurs/3]
#}

puts "Prix total pour $visiteurs visiteurs : $prix €, offert : $popcorns popcorns"

switch $visiteurs {
	1 { puts "Bienvenue, visiteur !" }
	2 -
	3 -
	4 { puts "Bienvenue, famille de visiteurs !" }
	default { puts "Bienvenue, groupe de visiteurs !" }
}

# afficher :
# 1 jour(s) : ... euros
# 2 jour(s) : ... euros
# ...
# 7 jour(s) : ... euros
for {set i 1} {$i<8} {incr i} {
	#set prixJour [expr $prix*$i]
	#puts "$i jour(s) : $prixJour euros"
	puts "$i jour(s) : [expr $prix*$i] euros"
}

#puts "------------------------"
# -- Solution while --
#set i 1
#while {$i <= 7} {
#	set prixJour [expr $prix*$i]
#	puts "$i jour(s) : $prixJour euros"
#	set i [expr $i+1]
#}

#after 5000